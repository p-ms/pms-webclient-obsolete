function Parser() {
    this.error = null;
    this.buffer = null;
    this.offset = 0;
};

Parser.prototype.parseMessage = function(message) {
    this.error = null;
    this.offset = 0;
    this.buffer = message;
    if (this.buffer[0] != "/") {
        this.error = "First element of message has to be a /";
        return undefined;
    }
    this.offset++;
    //parse the Command Token
    var name = this.parseToken();
    if (name === undefined) return undefined;
    var arguments = new Array();
    while (1) {
        this.consumeWhitespace();
        if (this.remainingLength() <= 0) break;
        var arg = undefined;
        var char = this.currentChar();
        if (char == "\"" || char == "'") arg = this.parseString();
        else if (char.match(/^[0-9|\+|\-|\.]$/)) arg = this.parseNumber();
        else {
            this.error = "Unexpected Elements";
            return undefined;
        }
        if (arg === undefined) {
            this.error = "Unknown Error";
            return undefined;
        }
        arguments.push(arg);
        this.nextChar();
    }
    return {
        'name': name,
        'arguments': arguments
    };
};

Parser.prototype.consumeWhitespace = function() {
    while (this.remainingLength() > 0 && this.currentChar().match(/\s/))
    this.nextChar();
};

Parser.prototype.parseToken = function() {
    var token = null;
    var firstChar = true;
    while (this.remainingLength() > 0) {
        var char = this.currentChar();
        if (char.match(/\s/)) return token;
        if (firstChar && char.match(/[A-Za-z_]/)) {
            firstChar = false;
            token = char;
            this.nextChar();
            continue;
        } else if (char.match(/[A-Za-z0-9_]/)) {
            token += char;
            this.nextChar();
            continue;
        }
        this.error = "A token can only contain the following chars: [A-Za-z0-9_] and can NOT start with a number";
        return undefined;
    }
    if (token.length) return token;
    this.error = "Empty token";
    return undefined;
};

Parser.prototype.parseNumber = function() {
    var number;
    var point = false;
    var char = this.currentChar();
    if (char.match(/^[0-9|\+|\-|\.]$/)) {
        number = char;
        if (char == '.') point = true;
    } else {
        this.error = "Invalid Begin of Number";
        return undefined;
    }
    while (this.hasNext()) {
        char = this.nextChar();
        if (char.match(/\s/)) break;
        else if (!char.match(/^[0-9|.]$/)) {
            this.error = "Invalid Part of Number";
            return undefined;
        }
        if (char == '.') {
            if (point) {
                this.error = "Numbers can only have one Point";
                return undefined;
            } else {
                point = true;
            }
        }
        number += char;
    }
    var num = NaN;
    if (!point) num = parseInt(number);
    else num = parseFloat(number);
    if (num == NaN) {
        this.error = "Javascript Number Parse Error";
        return undefined;
    }
    return num;
};

Parser.prototype.parseString = function() {
    var message = null;
    var quotes = this.currentChar();
    while (this.hasNext()) {
        var char = this.nextChar();
        if (char == "\\") continue;
        if (char == quotes) return message;
        if (message == null) message = char;
        else message += char;
    }
    this.error = "String is missing its end quotes";
    return undefined;
};

Parser.prototype.currentChar = function() {
    return this.buffer[this.offset];
};

Parser.prototype.nextChar = function() {
    this.offset++;
    return this.buffer[this.offset];
};

Parser.prototype.hasNext = function() {
    return (this.offset + 1 < this.buffer.length);
};

Parser.prototype.remainingLength = function() {
    return (this.buffer.length - this.offset);
};