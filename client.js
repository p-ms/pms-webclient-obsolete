  $(function() {

    var conn;
    var buffer = "";
    var msgLineEdit = $("#msg");
    var log = $("#log");
    var parser = new Parser();
    
    $( "#tabs" ).tabs({tabTemplate: "<li><a href='#{href}'>#{label}</a></li>"});
    
    function handleInvalidMessage(){
        receivedMessage("default","<b>Invalid Message: "+buffer+"</b>");
        buffer = "";
        return undefined;
    }
    
    function readNetstring(){
      var buflen = buffer.length;
      var delim = -1;
      for(var i = 0; i < buflen; i++){
        var cc = buffer.charCodeAt(i);
        if(cc == 0x3a){
          if(i == 0){
            return handleInvalidMessage();
          }
          delim = i;
          break;
        }
        if (cc < 0x30 || cc > 0x39){
            return handleInvalidMessage();
        }
      }
      
      
      if(delim > 0){
        var msgLenTxt = buffer.substr(0,delim);
        var msgLen = parseInt(msgLenTxt);
        if(msgLen === NaN){
            return handleInvalidMessage();
        }
      
        //if the buffer is shorter than the data we need stop
        if(msgLen+msgLenTxt.length+1+1 > buffer.length)
          return;
        
        if(buffer.charCodeAt(msgLenTxt.length+1+msgLen) != 0x2c){
          return handleInvalidMessage();
        }
        
        var message = buffer.substr(delim+1,msgLen);
        buffer = buffer.substr(msgLenTxt.length+1+msgLen+1);
        
        return message;
      }
      
      return undefined;
    }
    
    function toNetstring(str){
      return str.length+":"+str+",";
    }

    $("#form").submit(function() {
        if (!conn) {
            return false;
        }
        if (!msgLineEdit.val()) {
            return false;
        }
        
        var message = msgLineEdit.val();
        //msg.val(test);
        
        if(message.substr(0,1) != "/"){
          var curr = currentChannel();
          
          conn.send(toNetstring('/send "'+curr.slice(1)+'" "'+message+'"'));
        }else{
          conn.send(toNetstring(message));
        }
        msgLineEdit.val("");
        return false
    });

    if (window["WebSocket"]) {
        conn = new WebSocket("ws://localhost:8888");
        conn.onclose = function(evt) {
            receivedMessage("default","<b>Connection closed.</b>")
        }
        conn.onmessage = function(evt) {
          buffer+=evt.data;
          var str;
          while((str = readNetstring()) != undefined){
            var command = parser.parseMessage(str);
            
            if(parser.error)
              alert(parser.error);
            else
              handleCommand(command);
          }
        }
    } else {
        receivedMessage("default","<p><b>Your browser does not support WebSockets.</b></p>")
    }
    
    openWindow("default");
  });
  
  function receivedMessage(channel, msg) {
        var element = $("<pre class='chatline'/>").text(msg);
        var buffer = $("#"+channel);
        var d = buffer[0];
        var doScroll = (d.scrollTop == (d.scrollHeight - d.clientHeight));
        //alert ("Send to Buffer: "+channel);
        element.appendTo(buffer);
        //$("<br/>").appendTo(buffer);
        if (doScroll) {
            d.scrollTop = d.scrollHeight - d.clientHeight;
        }
    }
    
    function currentChannel (){
      return $( "#tabs ul li.ui-state-active a" ).attr("href");
    }
    
    function openWindow (channel){
      $( "#tabs" ).tabs( "add", "#"+channel, channel );
    }
    
    function closeWindow(channel){
        //alert("Try to close Channel: "+channel);
        var $tabs = $( "#tabs" );
        var $list = $tabs.find("li");
        var $element = $tabs.find("li a[href='#"+channel+"']");
        var index = $list.index($element.parent());
        $tabs.tabs( "remove", index );
    }
    